package com.journaldev.barcodevisionapi;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnTakePicture, btnScanBarcode;
    myDbAdapter helper;
    myDbAdapter myhelper;
    TableLayout mainLayout;
    public Context ctx = this;
    TableRow newRow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
    }

    private void RunFunction(){

    }

    private void initViews() {
        btnTakePicture = findViewById(R.id.btnTakePicture);
        btnScanBarcode = findViewById(R.id.btnScanBarcode);
        btnTakePicture.setOnClickListener(this);
        btnScanBarcode.setOnClickListener(this);
        helper = new myDbAdapter(this);
        mainLayout = findViewById(R.id.mainLayout);
        //final myDbAdapter.myDbHelper myhelper;
        final myDbAdapter.myDbHelper myhelper;
        myhelper = new myDbAdapter.myDbHelper(ctx);
        //helper.insertData("873227822728732", "Test overflow", "98ml", "Rp. 25000");
        TableLayout tl = (TableLayout) findViewById(R.id.mainLayout);

        if(helper.isTableEmpty()){
            TextView dataKosong = new TextView(this);
            dataKosong.setText("Data barang tidak tersedia");
            dataKosong.setGravity(Gravity.CENTER);
            tl.addView(dataKosong);
        }else{
            tl.removeAllViews();
            TableRow tr = new TableRow(this);
            tr.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView t1 = new TextView(this);
            t1.setText("Nama barang");
            t1.setGravity(Gravity.LEFT);
            t1.setTypeface(t1.getTypeface(), Typeface.BOLD);
            //t1.setBackgroundColor(Color.parseColor("#FF5733"));
            t1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            tr.addView(t1);

            TextView t3 = new TextView(this);
            t3.setText("Ukuran");
            t3.setGravity(Gravity.CENTER);
            t3.setTypeface(t3.getTypeface(), Typeface.BOLD);
            t3.setBackgroundColor(7);
            t3.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, 150));
            tr.addView(t3);

            TextView t2 = new TextView(this);
            t2.setText("Harga");
            t2.setTypeface(t3.getTypeface(), Typeface.BOLD);
            t2.setGravity(Gravity.RIGHT);
            t2.setBackgroundColor(7);
            t2.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            tr.addView(t2);

            TextView hapus = new TextView(this);
            //hapus.setText("&nbsp;");
            hapus.setPadding(0,0,150,0);
            hapus.setTypeface(t3.getTypeface(), Typeface.BOLD);
            hapus.setBackgroundColor(7);
            hapus.setGravity(Gravity.CENTER);
            hapus.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            tr.addView(hapus);


            tr.setBackgroundColor(Color.parseColor("#FF5733"));

            tl.addView(tr, new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));


            SQLiteDatabase db = myhelper.getWritableDatabase();
            String[] columns = {myDbAdapter.myDbHelper.UID, myDbAdapter.myDbHelper.barcodeBarang, myDbAdapter.myDbHelper.namaBarang, myDbAdapter.myDbHelper.hargaBarang, myDbAdapter.myDbHelper.ukuran};
            Cursor cursor = db.query(myDbAdapter.myDbHelper.TABLE_NAME, columns, null, null, null, null, null);
            StringBuffer buffer = new StringBuffer();
            while (cursor.moveToNext()) {
                int cid = cursor.getInt(cursor.getColumnIndex(myDbAdapter.myDbHelper.UID));
                final String barcodeBarang = cursor.getString(cursor.getColumnIndex(myDbAdapter.myDbHelper.barcodeBarang));
                final String namaBarang = cursor.getString(cursor.getColumnIndex(myDbAdapter.myDbHelper.namaBarang));
                String hargaBarang = cursor.getString(cursor.getColumnIndex(myDbAdapter.myDbHelper.hargaBarang));
                String ukuran = cursor.getString(cursor.getColumnIndex(myDbAdapter.myDbHelper.ukuran));

                TableRow tk = new TableRow(this);
                tk.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));

                TextView t7 = new TextView(this);
                t7.setText(namaBarang);
                t7.setGravity(Gravity.LEFT);
                //t1.setBackgroundColor(Color.parseColor("#FF5733"));
                t7.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
                tk.addView(t7);

                TextView t8 = new TextView(this);
                t8.setText(ukuran);
                t8.setGravity(Gravity.CENTER);
                //t8.setBackgroundColor(7);
                t8.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, 150));
                tk.addView(t8);

                TextView t9 = new TextView(this);
                t9.setText(hargaBarang);
                t9.setGravity(Gravity.RIGHT);
                //t9.setBackgroundColor(7);
                t9.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
                tk.addView(t9);

                Button btn = new Button(this);
                btn.setText("X");
                btn.setWidth(10);
                btn.setGravity(Gravity.CENTER);
                //t9.setBackgroundColor(7);
                btn.setLayoutParams(new TableRow.LayoutParams(10, TableRow.LayoutParams.WRAP_CONTENT));
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        helper.delete(barcodeBarang);
                        Message.message(ctx, "Barang " + namaBarang + " berhasil dihapus.");
                        TableLayout tl = (TableLayout) findViewById(R.id.mainLayout);
                        tl.removeAllViews();
                        initViews();

                    }
                });
                tk.addView(btn);

                tk.setBackgroundColor(Color.parseColor("#22ffff"));

                tl.addView(tk, new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));

            }

        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnTakePicture:
                startActivity(new Intent(MainActivity.this, PictureBarcodeActivity.class));
                break;
            case R.id.btnScanBarcode:
                startActivity(new Intent(MainActivity.this, ScannedBarcodeActivity.class));
                break;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        //initViews();
    }

}
