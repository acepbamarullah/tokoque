package com.journaldev.barcodevisionapi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class myDbAdapter {
    myDbHelper myhelper;
    public myDbAdapter(Context context)
    {
        myhelper = new myDbHelper(context);
    }

    public long insertData(String barcodeBarang, String namaBarang, String ukuran, String hargaBarang)
    {
        SQLiteDatabase dbb = myhelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(myDbHelper.barcodeBarang, barcodeBarang);
        contentValues.put(myDbHelper.namaBarang, namaBarang);
        contentValues.put(myDbHelper.ukuran, ukuran);
        contentValues.put(myDbHelper.hargaBarang, hargaBarang);
        long id = dbb.insert(myDbHelper.TABLE_NAME, null , contentValues);
        return id;
    }

    public String getDataWhereBarcode(String barcodeBarangs)
    {
        String countQuery = "SELECT  * FROM " + myDbHelper.TABLE_NAME +" where barcodeBarang='"+barcodeBarangs+"'";
        SQLiteDatabase db = myhelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        //SQLiteDatabase db = myhelper.getWritableDatabase();
        String[] columns = {myDbHelper.UID,myDbHelper.barcodeBarang,myDbHelper.namaBarang,myDbHelper.hargaBarang,myDbHelper.ukuran};
        //Cursor cursor =db.query(myDbHelper.TABLE_NAME,columns,null,null,null,null,null);
        StringBuffer buffer= new StringBuffer();
        while (cursor.moveToNext())
        {
            int cid = cursor.getInt(cursor.getColumnIndex(myDbHelper.UID));
            String barcodeBarang = cursor.getString(cursor.getColumnIndex(myDbHelper.barcodeBarang));
            String  namaBarang = cursor.getString(cursor.getColumnIndex(myDbHelper.namaBarang));
            String  hargaBarang = cursor.getString(cursor.getColumnIndex(myDbHelper.hargaBarang));
            String  ukuran = cursor.getString(cursor.getColumnIndex(myDbHelper.ukuran));
            buffer.append(cid+ "   " + barcodeBarang + "   " + namaBarang +"   "+ukuran+"   "+hargaBarang+" \n");
        }
        return buffer.toString();
    }

    public String getData()
    {
        SQLiteDatabase db = myhelper.getWritableDatabase();
        String[] columns = {myDbHelper.UID,myDbHelper.barcodeBarang,myDbHelper.namaBarang,myDbHelper.hargaBarang,myDbHelper.ukuran};
        Cursor cursor =db.query(myDbHelper.TABLE_NAME,columns,null,null,null,null,null);
        StringBuffer buffer= new StringBuffer();
        while (cursor.moveToNext())
        {
            int cid =cursor.getInt(cursor.getColumnIndex(myDbHelper.UID));
            String barcodeBarang =cursor.getString(cursor.getColumnIndex(myDbHelper.barcodeBarang));
            String  namaBarang =cursor.getString(cursor.getColumnIndex(myDbHelper.namaBarang));
            String  hargaBarang =cursor.getString(cursor.getColumnIndex(myDbHelper.hargaBarang));
            String  ukuran =cursor.getString(cursor.getColumnIndex(myDbHelper.ukuran));
            buffer.append(cid+ "   " + barcodeBarang + "   " + namaBarang +"   "+ukuran+"   "+hargaBarang+" \n");
        }
        return buffer.toString();
    }

    public  int delete(String barcodeBarang)
    {
        SQLiteDatabase db = myhelper.getWritableDatabase();
        String[] whereArgs ={barcodeBarang};

        int count =db.delete(myDbHelper.TABLE_NAME ,myDbHelper.barcodeBarang+" = ?",whereArgs);
        return  count;
    }

    public  boolean isTableEmpty()
    {
        String countQuery = "SELECT  * FROM " + myDbHelper.TABLE_NAME +"";
        SQLiteDatabase db = myhelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        if(count > 0){
            return false;
        }else{
            return true;
        }
    }

    public  boolean isDataExist(String barcodeBarang)
    {
        String countQuery = "SELECT  * FROM " + myDbHelper.TABLE_NAME +" where barcodeBarang='"+barcodeBarang+"'";
        SQLiteDatabase db = myhelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        if(count > 0){
            return true;
        }else{
            return false;
        }
    }

    public int updateName(String newName , String barcodeBarang)
    {
        SQLiteDatabase db = myhelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(myDbHelper.namaBarang,newName);
        String[] whereArgs= {barcodeBarang};
        int count =db.update(myDbHelper.TABLE_NAME,contentValues, myDbHelper.barcodeBarang+" = ?",whereArgs );
        return count;
    }

    static class myDbHelper extends SQLiteOpenHelper
    {
        private static final String DATABASE_NAME = "myDatabase";    // Database Name
        public static final String TABLE_NAME = "TabelBarang";   // Table Name
        private static final int DATABASE_Version = 1;    // Database Version
        public static final String UID="_id";     // Column I (Primary Key)
        public static final String namaBarang = "namaBarang";    //Column II
        public static final String hargaBarang = "hargaBarang";    //Column II
        public static final String barcodeBarang = "barcodeBarang";    //Column II
        public static final String ukuran = "ukuran";    //Column II
        private static final String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME+
                " ("+UID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+namaBarang+" VARCHAR(255) ,"+ hargaBarang+" VARCHAR(225), "+barcodeBarang+" VARCHAR(255), "+ukuran+" VARCHAR(255));";
        private static final String DROP_TABLE ="DROP TABLE IF EXISTS "+TABLE_NAME;
        private Context context;

        public myDbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_Version);
            this.context=context;
        }

        public void onCreate(SQLiteDatabase db) {

            try {
                db.execSQL(CREATE_TABLE);
                Message.message(context,"Berhasil bikin database");
            } catch (Exception e) {
                Message.message(context,""+e);
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            try {
                Message.message(context,"OnUpgrade");
                db.execSQL(DROP_TABLE);
                onCreate(db);
            }catch (Exception e) {
                Message.message(context,""+e);
            }
        }
    }
}
