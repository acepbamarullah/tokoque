package com.journaldev.barcodevisionapi;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.SparseArray;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

public class ScannedBarcodeActivity extends AppCompatActivity {


    SurfaceView surfaceView;
    TextView txtBarcodeValue, txtKodeBarcode, txtNamaBarang, txtHargaBarang, txtUkuran,txtInstruksi;
    LinearLayout surfaceArea;
    private BarcodeDetector barcodeDetector;
    private CameraSource cameraSource;
    private static final int REQUEST_CAMERA_PERMISSION = 201;
    Button btnAction;
    String intentData = "";
    boolean isEmail = false;
    public Context ctx = this;
    myDbAdapter helper;
    myDbAdapter myhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_barcode);

        initViews();
    }

    private void initViews() {
        helper = new myDbAdapter(this);

        txtBarcodeValue = findViewById(R.id.txtBarcodeValue);
        txtNamaBarang = findViewById(R.id.txtNamaBarang);
        txtHargaBarang = findViewById(R.id.txtHargaBarang);
        txtKodeBarcode = findViewById(R.id.txtKodeBarcode);
        txtUkuran = findViewById(R.id.txtUkuran);
        txtInstruksi = findViewById(R.id.txtInstruksi);
        surfaceView = findViewById(R.id.surfaceView);
        btnAction = findViewById(R.id.btnAction);
        //surfaceArea = findViewById(R.id.surfaceArea);
        LinearLayout layout = findViewById(R.id.surfaceArea);
        ViewGroup.LayoutParams params = layout.getLayoutParams();
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y / 3;
        params.height = height;
        params.width = width;
        layout.setLayoutParams(params);
        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String barcode = txtKodeBarcode.getText().toString();
                String namabarang = txtNamaBarang.getText().toString();
                String ukuran = txtUkuran.getText().toString();
                String harga = txtHargaBarang.getText().toString();
                helper.insertData(barcode, namabarang, ukuran, "Rp. "+harga);
                Message.message(ctx,"Berhasil simpan data baru");
                startActivity(new Intent(ScannedBarcodeActivity.this, MainActivity.class));
                finish();

            }
        });
    }

    private void initialiseDetectorsAndSources() {

        Toast.makeText(getApplicationContext(), "Barcode scanner started", Toast.LENGTH_SHORT).show();

        barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.ALL_FORMATS)
                .build();

        cameraSource = new CameraSource.Builder(this, barcodeDetector)
                .setRequestedPreviewSize(1920, 1080)
                .setAutoFocusEnabled(true) //you should add this feature
                .build();
        surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if (ActivityCompat.checkSelfPermission(ScannedBarcodeActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        cameraSource.start(surfaceView.getHolder());
                        //surfaceView.getHolder().setFixedSize(surfaceView.getWidth(), surfaceView.getHeight() / 2);
                    } else {
                        ActivityCompat.requestPermissions(ScannedBarcodeActivity.this, new
                                String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });


        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
                Toast.makeText(getApplicationContext(), "To prevent memory leaks barcode scanner has been stopped", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                final myDbAdapter helper;
                final myDbAdapter.myDbHelper myhelper;

                helper = new myDbAdapter(ctx);
                myhelper = new myDbAdapter.myDbHelper(ctx);
                if (barcodes.size() != 0) {

                    txtBarcodeValue.post(new Runnable() {

                        @Override
                        public void run() {
                            if (barcodes.valueAt(0).email != null) {
                                txtBarcodeValue.removeCallbacks(null);
                                intentData = barcodes.valueAt(0).email.address;
                                txtBarcodeValue.setText(intentData);
                                isEmail = true;
                                btnAction.setText("ADD CONTENT TO THE MAIL");
                            } else {
                                isEmail = false;
                                btnAction.setText("Simpan data");
                                intentData = barcodes.valueAt(0).displayValue;
                                txtBarcodeValue.setText(intentData);
                                txtKodeBarcode.setText(intentData);
                                if(helper.isDataExist(intentData)){
                                    String countQuery = "SELECT  * FROM " + myDbAdapter.myDbHelper.TABLE_NAME +" where barcodeBarang='"+intentData+"'";
                                    SQLiteDatabase db = myhelper.getReadableDatabase();
                                    Cursor cursor = db.rawQuery(countQuery, null);
                                    //SQLiteDatabase db = myhelper.getWritableDatabase();
                                    String[] columns = {myDbAdapter.myDbHelper.UID, myDbAdapter.myDbHelper.barcodeBarang, myDbAdapter.myDbHelper.namaBarang, myDbAdapter.myDbHelper.hargaBarang, myDbAdapter.myDbHelper.ukuran};
                                    //Cursor cursor =db.query(myDbHelper.TABLE_NAME,columns,null,null,null,null,null);
                                    StringBuffer buffer= new StringBuffer();
                                    while (cursor.moveToNext())
                                    {
                                        int cid = cursor.getInt(cursor.getColumnIndex(myDbAdapter.myDbHelper.UID));
                                        String barcodeBarang = cursor.getString(cursor.getColumnIndex(myDbAdapter.myDbHelper.barcodeBarang));
                                        String  namaBarang = cursor.getString(cursor.getColumnIndex(myDbAdapter.myDbHelper.namaBarang));
                                        String  hargaBarang = cursor.getString(cursor.getColumnIndex(myDbAdapter.myDbHelper.hargaBarang));
                                        String  ukuran = cursor.getString(cursor.getColumnIndex(myDbAdapter.myDbHelper.ukuran));
                                        txtNamaBarang.setText(namaBarang);
                                        txtHargaBarang.setText(hargaBarang);
                                        txtKodeBarcode.setText(barcodeBarang);
                                        txtUkuran.setText(ukuran);
                                        txtInstruksi.setVisibility(View.GONE);
                                    }
                                }else{
                                    Message.message(ctx,"Data barang tidak tersedia. Lengkapi data barang, kemudian klik simpan.");
                                    txtNamaBarang.setEnabled(true);
                                    txtNamaBarang.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                                    txtNamaBarang.setHint("Silakan diisi");
                                    txtHargaBarang.setEnabled(true);
                                    txtHargaBarang.setHint("Silakan diisi");
                                    txtHargaBarang.setInputType(InputType.TYPE_CLASS_NUMBER);
                                    txtKodeBarcode.setText(intentData);
                                    txtUkuran.setEnabled(true);
                                    txtUkuran.setHint("Silakan diisi");
                                    btnAction.setVisibility(View.VISIBLE);
                                    txtInstruksi.setVisibility(View.GONE);
                                }
                            }
                        }
                    });

                }
            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();
        cameraSource.release();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initialiseDetectorsAndSources();


    }
}
